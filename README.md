<br><br>

<h3>lb-vue-tree</h3>

<br>
<p><img src="https://www.leapbit.com/branding/logo.png" style="width: 300px;"></p>

<br>

## Screenshot

<p><img src="https://gitlab.com/leapbit-public/lb-vue-tree/raw/master/screen.png" alt="lb-vue-tree" ></p>

## Description

<p align="center">
  Vue2.js boostrap 4.0 tree with serverside loading, checkbox option and drag and drop support
</p>

<br><br><br><br><br>

## Install

``` bash
npm install lb-vue-tree --save
```

Include plugin in your `main.js` file.

```js
import Vue from 'vue'
import lbVueTree from 'lb-vue-tree';
Vue.component('lb-vue-tree', lbVueTree);
```

## Usage

# Local data (or complete serverside load)

In this example, if you have a call to server, or static data that is smaller and has all the nodes inside, you can fetch data once and use it in a tree. 

template code for local data

```html
<template>
    <lb-vue-tree v-model="data" v-on:itemclick="itemClick" display-field="text">
        <template slot="rootitem"slot-scope="{item, parent}">
            <span>Root item</span>
        </template>
        <template slot="menu" slot-scope="{item, parent}">
            <ul>
                <li>New</li>
                <li>Edit</li>
            </ul>
        </template>
        <template v-slot:itemcontent="{item}">
            <div class="item-inputs">
                Min: <input type="text" v-model="item.text">
                Max: <input type="text">
                Active: <input type="checkbox">
            </div>
        </template>
    </lb-vue-tree>
</template>
```
<p>Three optional slots are available for use:</p>
<ul>
<li> "rootitem": displaying custom tree root item in case it is not supplied by your data, but you need a starting point, </li>
<li>"menu": showing menu on right click. Accepts two arguments - item and parent, that are passed to menu slot,</li>
<li>"itemcontent": showing some additional content refering to tree items.</li>
</ul>


script code for local data

```js
export default {
    data(){
        return {
            data: [{
                id: '1',
                text: 'test',
                children: [{
                    id: '2',
                    text: 'child test'
                }]
            }]
        }
    },
    methods: {
        itemClick(item, parent) {

        }
    }
};
```
<br><br>

# Remote data (server side data), fetching node by node, also draggable support

In this case you can load server side data only for singe node, so for example you will load only initial root level, then by
clicking on the nodes your assigned method will be called to fetch "ONLY" that node children

Method for fetching node data MUST be a promise, callbacks are not supported

template code for server side data

```html
<template>
    <lb-vue-tree v-model="data" :async-method="getData" v-on:itemclick="itemClick" :draggable="true" v-on:dragend="dragEnd" display-field="username">
        <template slot="rootitem">
            <span>Root item</span>
        </template>
        <template slot="menu" slot-scope="{item, parent}">
            <ul>
                <li>New</li>
                <li>Edit</li>
            </ul>
        </template>
    </lb-vue-tree>
</template>
```
There are two optional slots to use:
"rootitem": used if you wan't to display root item of the tree in case your tree data does not have it and you need a starting point
"menu": if you need a menu on right click, you can add it here, there are also two arguments, item and parent passed to menu slot


script code for remote data

```js
export default {
    data(){
        return {
            data: []
        }
    },
    async created()
    {
        //make an initial load of data
        this.data = await this.getData(null);
    },
    methods: {
        itemClick(item) {

        },
        dragEnd(data)
        {
            //data.start holds the item you are dragging
            //data.end holds the item dragged on
            //data.position holds the postion related to end position (upper, center, lower)
            console.log(data.start, data.end, data.position);
        },
        async getData(item, parent)
        {
            if(item != null)
            {
                //fetch data from server for specific node based on data from item, for example:
                return await axios.get('/getTreedata?id='+item.id)
            }
            else
            {
                return await axios.get('/getTreedata')
            }
        }
    }
};
```
<br><br>


## Available settings

| Property | Type | Required | Description |
| :---------------- | :-- | :-- | :-- |
| value (v-model) | Array | * | value array |
| display-field | string | | Display field key from value item object (Defaults to text) |
| display-func | function | | If you want to combine multiple fields to display text, you can specify custom function which receives one argument (item) |
| children-field | string | | Children field key (key which holds descendants) from value item object (Defaults to children) |
| children-expanded-field | string | | If you want to show this branch already expanded set this to boolean field (Defaults to expanded) |
| has-children-field | string | | In case of async tree, if you already know if that branch has children set the filed that holds the boolean here |
| checkbox | boolean | | If you want to enable checkboxes in the tree (Defaults to false) |
| checked-field | string | | Key from value which holds boolean state if the item is checked or not |
| async-method | function | | If you want to use serverside fetching node by node (in case of big tree) method receives item as argument |
| draggable | boolean | | If you want to enable drag and drop, put boolean true here (not string) |
| clickonopen | boolean | | If you want to enable item click event on openning of the item |
| showvalue | boolean | | If you have a display function and you don't wan't to display value also, put this to false |
| showsearch | boolean | | If you want to have search boxes for each level enabled |
| searchPlaceholder | string | | String to use for search placeholder on each item, you can use {level} specifier inside string |

## Available events

| Event | Arguments | Description |
| :---------------- | :-- | :-- |
| itemclick | item, parentItem | item from data array you clicked on and it's parent |
| itemrightclick | item, parentItem | item from data array you clicked on with right mouse button and it's parent |
| dragend | data | Function to be called when drag ends, one argument, object with start, end and position attributes |

<br><br>