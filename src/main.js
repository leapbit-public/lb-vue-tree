// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import Tree from './Tree.vue';
import Dev from './Dev.vue';
// import Tree from '../dist/lb-vue-tree'

Vue.component('lb-vue-tree', Tree);

new Vue({
    render: (h) => h(Dev),
}).$mount('#app');
